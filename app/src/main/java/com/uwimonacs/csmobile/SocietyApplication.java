package com.uwimonacs.csmobile;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;
import com.uwimonacs.csmobile.classes.models.Event;
import com.uwimonacs.csmobile.classes.models.EventType;
import com.uwimonacs.csmobile.classes.models.Payment;
import com.uwimonacs.csmobile.classes.models.Profile;

/**
 * @author Javon Davis
 *         Created by Javon Davis on 28/10/2015.
 */
public class SocietyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        ParseObject.registerSubclass(Event.class);
        ParseObject.registerSubclass(EventType.class);
        ParseObject.registerSubclass(Payment.class);
        ParseObject.registerSubclass(Profile.class);

        Parse.initialize(this, getString(R.string.app_id), getString(R.string.client_id));
    }



}
