package com.uwimonacs.csmobile.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseUser;
import com.uwimonacs.csmobile.R;
import com.uwimonacs.csmobile.classes.models.User;
import com.uwimonacs.csmobile.dialogs.LoginDialogFragment;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LoginDialogFragment.OnLoginListener{

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.fab) FloatingActionButton fab;
    @Bind(R.id.drawer_layout) DrawerLayout drawer;
    @Bind(R.id.nav_view) NavigationView navigationView ;
    @Bind(R.id.home_coordinator) CoordinatorLayout coordinatorLayout;
    private TextView usernameView;

//    @Bind(R.id.email) TextView emailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        initializeDrawer();
        initializeListeners();
        checkUserLogin();

    }

    private void initializeListeners() {
        fab.setOnClickListener(view -> Snackbar.make(view, "This will be the button to add a new post", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show());
    }

    private void initializeDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        LinearLayout headerLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_header_home, null);
        navigationView.setNavigationItemSelectedListener(this);
        //navigationView.inflateMenu(R.menu.activity_home_drawer_logged_out);
        usernameView = (TextView) headerLayout.findViewById(R.id.username_display);
        navigationView.addHeaderView(headerLayout);
    }

    private void disableMenuItems(NavigationView navigationView) {
        Menu navMenu = navigationView.getMenu();

        List<MenuItem> items = Arrays.asList(navMenu.findItem(R.id.nav_marathons), navMenu.findItem(R.id.nav_gallery));

        for (MenuItem item : items) {
            SpannableString spanString = new SpannableString(item.getTitle().toString());
            spanString.setSpan(new ForegroundColorSpan(Color.LTGRAY), 0, spanString.length(), 0);

            item.setTitle(spanString);
            item.setEnabled(false);
        }
    }

    public void checkUserLogin() {
        final User user = new User();
        boolean isLoggedIn = user.existingUser();

        if (isLoggedIn) {
            if (usernameView != null) {
                usernameView.setText(user.getUsername());
            }
            showLoggedInMenu();
        } else {
            restrictOptions();
        }
    }

    private void restrictOptions() {
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_home_drawer_logged_out);
        // restrict other guest actions
    }

    private void showLoggedInMenu()
    {
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_home_drawer_logged_in);
    }

    public void onLoginClick(MenuItem item)
    {
        LoginDialogFragment loginDialog= new LoginDialogFragment();
        loginDialog.show(getFragmentManager(), "Log in");
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_blog:
                // Handle the camera action
                break;
            case R.id.nav_gallery:

                break;
            case R.id.nav_marathons:

                break;
            case R.id.nav_settings:

                break;
            case R.id.nav_log_out:
                if (User.isLoggedIn()) {
                    ParseUser.logOut();
                    restrictOptions();
                }
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void attemptLogin(String username, String password) {
        User user = new User();
        ParseUser.logInInBackground(username, password, (mUser, e) -> {
                if (mUser != null) {
                    user.setUser(mUser);
                    assert usernameView != null;
                    usernameView.setText(user.getUsername());
                    showLoggedInMenu();
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                    Snackbar.make(coordinatorLayout,"Log in Failed",Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();

                }
            });
    }

    @Override
    public void forgotPassword() {

    }
}
