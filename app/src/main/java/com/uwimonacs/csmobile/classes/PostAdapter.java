package com.uwimonacs.csmobile.classes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uwimonacs.csmobile.R;

/**
 * @author Javon Davis
 *         Created by Javon Davis on 29/10/2015.
 */
public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);
        RecyclerView.ViewHolder vh = new PostViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class PostViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView picture;
        public TextView title,author,date,content;

        public PostViewHolder(View itemView)
        {
            super(itemView);
            picture = (ImageView) itemView.findViewById(R.id.post_picture);
            title = (TextView) itemView.findViewById(R.id.post_title);
            author = (TextView) itemView.findViewById(R.id.post_author);
            date = (TextView) itemView.findViewById(R.id.post_date);
            content = (TextView) itemView.findViewById(R.id.post_content);

        }
    }

}
