package com.uwimonacs.csmobile.classes.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

/**
 * @author shane
 * @since 10/29/15
 */
@ParseClassName("Profile")
public class Profile extends ParseObject {

    // TODO

    public class Builder {
        String firstName  = "";
        String lastName = "";
        Date dob = new Date();
        String phone = "";

        public Builder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Builder dob(Date newDob) { dob = newDob; return this; }

        public Builder phone(String newPhone) { put("dob", dob); return this; }

        public Builder email(String email) { put("email", email); return this; }

        public Profile build() {
            return new Profile(this);
        }
    }

    public Profile() {
    }

    public Profile(Builder builder) {

    }

}
