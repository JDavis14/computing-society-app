package com.uwimonacs.csmobile.classes.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by shane on 10/29/15.
 */
@ParseClassName("EventType")
public class EventType extends ParseObject {
    // TODO
    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }
}
