package com.uwimonacs.csmobile.classes.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

/**
 * Created by shane on 10/29/15.
 */
@ParseClassName("Event")
public class Event extends ParseObject {
    // TODO


    public Event() {}

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public Date getDate() {
        return getDate("date");
    }

    public void setDate(Date date) {
        put("date", date);
    }
}
