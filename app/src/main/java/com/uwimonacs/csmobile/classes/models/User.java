package com.uwimonacs.csmobile.classes.models;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * @author Javon Davis
 *         Created by Javon Davis on 28/10/2015.
 */
public class User {

    private ParseUser user;
    private String firstName,lastName,name,email,username;

    public User()
    {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            setUser(currentUser);
        }
    }

    public User(ParseUser user)
    {
        setUser(user);
    }

    public boolean existingUser()
    {
        return user != null;
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public ParseUser getUser() {
        return user;
    }

    public void setUser(ParseUser user) {
        this.user = user;
        setUsername(user.getUsername());
        setFirstName((String) user.get("first_name"));
        setLastName((String) user.get("last_name"));
        setName(getFirstName() + " " + getLastName());
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    private void setUsername(String username) {
        this.username = username;
    }

    public static boolean isLoggedIn() {
        User user = new User();
        return user.existingUser();
    }
}
