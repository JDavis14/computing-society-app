package com.uwimonacs.csmobile.classes;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessOnScrollRecycler extends RecyclerView.OnScrollListener {

    boolean mloading;
    private final int AUTO_LOAD_THRESHOLD=4;
    private LinearLayoutManager mlinearLayoutManager;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    PostAdapter postAdapter;

    public EndlessOnScrollRecycler(LinearLayoutManager linearLayoutManager,PostAdapter adapter){
        this.mlinearLayoutManager=linearLayoutManager;
        this.postAdapter=adapter;
    }

    public void setLoading(boolean loading) {
        this.mloading = loading;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        visibleItemCount=recyclerView.getChildCount();
        totalItemCount=mlinearLayoutManager.getItemCount();
        firstVisibleItem=mlinearLayoutManager.findLastVisibleItemPosition();
        if (!mloading){
            if (totalItemCount-AUTO_LOAD_THRESHOLD<=firstVisibleItem+visibleItemCount){
                loading();
                setLoading(true);
            }
        }
    }

    protected abstract void loading();

    public void refreshAdapter(){
        postAdapter.notifyDataSetChanged();
    }
}