package com.uwimonacs.csmobile.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.uwimonacs.csmobile.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class SessionFragment extends Fragment {

    @Bind(R.id.email) EditText emailBtn;
    @Bind(R.id.password) EditText passwordBtn;
    @Bind(R.id.sign_in_button) Button signInBtn;

    public SessionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_session, container, false);
        ButterKnife.bind(this, view);

        return view;
    }
}
