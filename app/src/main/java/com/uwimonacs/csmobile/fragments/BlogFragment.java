package com.uwimonacs.csmobile.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.uwimonacs.csmobile.R;
import com.uwimonacs.csmobile.classes.EndlessOnScrollRecycler;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnPostSelectedListener} interface
 * to handle interaction events.
 * Use the {@link BlogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlogFragment extends Fragment {

    private OnPostSelectedListener mListener;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    /**
     *
     *
     * @return A new instance of fragment BlogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlogFragment newInstance() {
        BlogFragment fragment = new BlogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public BlogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_home, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.posts);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mAdapter);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addOnScrollListener(new EndlessOnScrollRecycler(linearLayoutManager, mAdapter) {
            @Override
            protected void loading() {
                try {
                    loadmore();
                } catch (Exception e1) {
                    e1.printStackTrace();
                } finally {
                    setLoading(false);
                    refreshAdapter();
                }
            }
        });
        return view;
    }

    private void loadmore() throws ParseException {
        ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("posts");
        parseQuery.setLimit(5);
        parseQuery.findInBackground((list, e) -> {
            if (list!=null &&!list.isEmpty()){
                for (ParseObject obj:list){

                    Posts mpost=new Posts();
                    //Looping through the results and setting the values to mpost
                    Container.getInstance().add(mpost);
                    Container.getInstance().setLastdate(obj.getCreatedAt());
                    //Container is a singleton class containing a arraylist
                }
            }
        });

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPostSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPostSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Listener
     */
    public interface OnPostSelectedListener {
        void onPostSelected(ParseObject post);
    }

}
