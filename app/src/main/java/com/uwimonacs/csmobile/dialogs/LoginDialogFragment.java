package com.uwimonacs.csmobile.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.uwimonacs.csmobile.R;

import butterknife.Bind;


/**
 * @author javon
 */
public class LoginDialogFragment extends DialogFragment {

    private OnLoginListener mListener;
    EditText usernameField;
    EditText passwordField;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            mListener = (OnLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_login_dialog,null);

        usernameField = (EditText) view.findViewById(R.id.username_field);
        passwordField = (EditText) view.findViewById(R.id.password_field);

        builder.setView(view)
                .setMessage(R.string.log_in)
                .setPositiveButton(R.string.log_in, (dialog, id) -> {
                    //log in user

                    mListener.attemptLogin(usernameField.getText().toString().trim(),
                            passwordField.getText().toString().trim());
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // User cancelled the dialog
                });

        return builder.create();
    }

    public interface OnLoginListener {
        void attemptLogin(String username, String password);
        void forgotPassword();
    }
}
