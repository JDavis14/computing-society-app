
// These two lines are required to initialize Express in Cloud Code.
 express = require('express');
 app = express();

// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(require('connect').bodyParser());    // Middleware for reading request body
var parseExpressCookieSession = require('parse-express-cookie-session');

// In your middleware setup...
 app.use(express.cookieParser('secret'));
app.use(parseExpressCookieSession({ cookie: { maxAge: 3600000 } }));

// This is an example of hooking up a request handler with a specific request
// path and HTTP verb using the Express routing API.
app.get('/hello', function(req, res) {
  res.render('hello', { message: 'Congrats, you just set up your app!' });
});
app.get('/', function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser) {
		console.log(currentUser.username);
    // do stuff with the user
    res.render('hello', { message: 'Congrats, you logged in!' });
} else {
    console.log("fail:"+req.user);
    res.render('index', { choice: 'Login' });
}
  
});

app.get('/home', function(req, res) {
	var currentUser = Parse.User.current();

	if (currentUser) {
		console.log(currentUser.username);

		var Update = Parse.Object.extend("Update");
		var query = new Parse.Query(Update);
		var data;
		query.find({
		    success:function(results) {
		            console.log("Total: "+results);
		            data = results;
		            currentUser.fetch().then(function(fetchedUser){
					    var name = fetchedUser.getUsername();
					    res.render('home', { username: name,  updates: data });
					}, function(error){
					    //Handle the error
					});
		            
		        },
		        error:function(error) {
		        alert("Error when getting objects!");
		        }

		        
    });
    // do stuff with the user

} else {
    
    res.redirect('/');
}
  
});

app.post("/login", function(req, res) {
  Parse.User.logIn(req.body.username, req.body.password).then(function() {
    // Login succeeded, redirect to homepage.
    // parseExpressCookieSession will automatically set cookie.
    console.log("Success:"+req.body.username+"|"+req.body.password);
    res.redirect('/home');
  },
  function(error) {
  	console.log("Fail:"+req.body.username+"|"+req.body.password);
    // Login failed, redirect back to login form.
    res.redirect("/");
  });
});

app.get("/logout", function(req, res) {
  var currentUser = Parse.User.current();
	if (currentUser) {
	    Parse.User.logOut();
	    res.redirect('/');
	} else {
	    res.redirect('/');
	}
});

// // Example reading from the request query string of an HTTP get request.
// app.get('/test', function(req, res) {
//   // GET http://example.parseapp.com/test?message=hello
//   res.send(req.query.message);
// });

// // Example reading from the request body of an HTTP post request.
// app.post('/test', function(req, res) {
//   // POST http://example.parseapp.com/test (with request body "message=hello")
//   res.send(req.body.message);
// });

// Attach the Express app to Cloud Code.
app.listen();
